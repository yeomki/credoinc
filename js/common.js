window.onbeforeunload = function () {
  window.scrollTo(0, 0);
}

// Device Check
var isMobile = {
	Android: function() {
		return navigator.userAgent.match(/Android/i);
	},
		BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
	},
		iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
		Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
	},
		Windows: function() {
		return navigator.userAgent.match(/IEMobile/i);
	},
		any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};

;(function($) {
	
	// Content Interative
	var contentWayPoint = function() {
		var i = 0;
		$('.page-animate').waypoint( function( direction ) {

			if( direction === 'down' && !$(this.element).hasClass('page-animated') ) {
				
				i++;

				$(this.element).addClass('item-animate');

				setTimeout(function(){

					$('body .page-animate.item-animate').each(function(k){
						var el = $(this);
						setTimeout( function () {
							var effect = el.data('animate-effect');
							if ( effect === 'fadeIn') {
								el.addClass('fadeIn page-animated');
							} else if ( effect === 'fadeInLeft') {
								el.addClass('fadeInLeft page-animated');
							} else if ( effect === 'fadeInRight') {
								el.addClass('fadeInRight page-animated');
							} else {
								el.addClass('fadeInUp page-animated');
							}
							el.removeClass('item-animate');
						},  k * 100, 'easeInOutExpo' );
					});
					
				}, 100);
				
			}

		} , { offset: '95%' } );
	};

	var navbarState = function() {
		var lastScrollTop = 0;
		$(window).scroll(function(){
			
			var $this = $(this),
				 	st = $this.scrollTop(),
				 	navbar = $('.page-navbar:not(.non-effect)');

			if ( st > 200 ) {
				navbar.addClass('scrolled');
			} else {
				navbar.removeClass('scrolled awake');
			}

			if ( navbar.hasClass('scrolled') && st > 300 ) {
		   	if (st > lastScrollTop){
		      	navbar.addClass('awake');
		      	navbar.removeClass('sleep');
		   	} else {
		      	navbar.addClass('awake');
		      	navbar.removeClass('sleep');
		   	}
		   	lastScrollTop = st;
		  }

		}).one("load", function(){
			$(this).scroll();
		});
	};

	
	
	
	var stellarInit = function() {
		if( !isMobile.any() ) {
			$(window).stellar();
		}
	};


	// Page Nav
	var clickMenu = function() {
		$('.navbar-sub-nav a:not([class="external"])').on('click', function(event){
			var section = $(this).data('nav-section'),
			
			navbar = $('.navbar-sub-nav');
			
			/*if (isMobile.any()) {
				$('.navbar-toggle').click();
			}*/
			
			if ( $('[data-section="' + section + '"]').length ) {
				$('html, body').animate({
					scrollTop: $('[data-section="' + section + '"]').offset().top - ($('.navbar-default').height() + 10)
				}, 500, 'easeInOutExpo');


			}
			
			var i = 0;
			
			$('[data-section="' + section + '"]').find('.page-animate').waypoint( function( direction ) {

				if(!$(this.element).hasClass('page-animated') ) {

					i++;

					$(this.element).addClass('item-animate');

					setTimeout(function(){

						$('[data-section="' + section + '"]').find('.page-animate.item-animate').each(function(k){
							var el = $(this);
							setTimeout( function () {
								var effect = el.data('animate-effect');
								if ( effect === 'fadeIn') {
									el.addClass('fadeIn page-animated');
								} else if ( effect === 'fadeInLeft') {
									el.addClass('fadeInLeft page-animated');
								} else if ( effect === 'fadeInRight') {
									el.addClass('fadeInRight page-animated');
								} else {
									el.addClass('fadeInUp page-animated');
								}
								el.removeClass('item-animate');
							},  k * 100, 'easeInOutExpo' );
						});

					}, 100);

				}

			} , { offset: '90%' } );
			

			return false;
			e.preventDefault();
		});


	};

	// Navigation
	var navActive = function(section) {
		var $el = $('.navbar-sub-nav');
		$el.find('li').removeClass('active');
		$el.each(function(){
			$(this).find('a[data-nav-section="'+section+'"]').closest('li').addClass('active');
		});

	};

	var navigationSection = function() {

		var $section = $('section[data-section]');
		
		$section.waypoint(function(direction) {
		  	if (direction === 'down') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
	  		offset: $section.offset().top
		});

		$section.waypoint(function(direction) {
		  	if (direction === 'up') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
		  	offset: function() { return -($(this.element).height() - $section.offset().top)}
		});

	};


	var smoothScroll = function() {
		var $root = $('html, body');

		$('.smoothscroll').on("click", function () {
			$root.animate({
		    scrollTop: $( $.attr(this, 'data-target') ).offset().top
			}, 500);
			return false;
		});
	};
	
	$(function(){

		// FakeLoader
        $(".fakeloader").fakeLoader({
            timeToHide:100000,
            bgColor:"#000",
            spinner:"spinner2"
        });

        $(window).load(function(){
	        $(".fakeloader").hide();

	        setTimeout(function(){
	        	contentWayPoint();
	        }, 50);
        })

        

        
		stellarInit();
		smoothScroll();
		navbarState();

		if($("#navbarSubNav").length) {
			clickMenu();
			navigationSection();

	        $(window).scroll(function(){
	       		var positionCheck = $('.page-navbar').offset().top + $('.page-navbar').height();
			    
			    if ($("#navbarSubNav").offset().top <= positionCheck){
			        $("#navbarSubNav .inner").addClass('fixed');
			    } else {
			        $("#navbarSubNav .inner").removeClass('fixed');
			        navActive($('[data-section]').eq(0).data('section'));
			    }
			});
    	}

		// Google map
		var marker;
        var mapLocation = new google.maps.LatLng('37.569635', '126.974880');
        var markLocation = new google.maps.LatLng('37.569635', '126.974880'); 
        
        if(document.getElementById("map-canvas")){
			function initialize() {
				var mapOptions = {
				  center: mapLocation, 
				  zoom: 18,
				  mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(document.getElementById("map-canvas"), 
				    mapOptions);
				 
				var size_x = 60; 
				var size_y = 60; 
				 
				var image = new google.maps.MarkerImage( 'http://www.larva.re.kr/home/img/boximage3.png',
				new google.maps.Size(size_x, size_y),
				'',
				'',
				new google.maps.Size(size_x, size_y));
				 

				marker = new google.maps.Marker({
				       position: markLocation, 
				       map: map,
				       icon: image, 
				       title: '(주)크레도아이엔씨' 
				});
		 
				var content = "(주)크레도아이엔씨"; 

				var infowindow = new google.maps.InfoWindow({ content: content});

				infowindow.open(map,marker);
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		}

	});

})(window.jQuery);